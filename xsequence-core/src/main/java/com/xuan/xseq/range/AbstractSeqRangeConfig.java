package com.xuan.xseq.range;

/**
 * 区间配置基类
 * Created by xuan on 2018/4/29.
 */
public class AbstractSeqRangeConfig {

    /**
     * 区间步长
     */
    private int  step       = 1000;
    /**
     * 区间起始位置，真实从stepStart+1开始
     */
    private long stepStart  = 0;
    /**
     * 获取区间失败重试次数
     */
    private int  retryTimes = 100;

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getRetryTimes() {
        return retryTimes;
    }

    public void setRetryTimes(int retryTimes) {
        this.retryTimes = retryTimes;
    }

    public long getStepStart() {
        return stepStart;
    }

    public void setStepStart(long stepStart) {
        this.stepStart = stepStart;
    }
}
