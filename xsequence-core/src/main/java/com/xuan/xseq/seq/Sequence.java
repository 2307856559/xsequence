package com.xuan.xseq.seq;

import com.xuan.xseq.exception.SequenceException;
import com.xuan.xseq.range.SeqRangeMgr;

/**
 * 获取序列号通用接口
 * Created by xuan on 2018/1/10.
 */
public interface Sequence {

    /**
     * 取下一个值
     *
     * @return 序列值
     * @throws SequenceException 异常
     */
    long nextValue() throws SequenceException;

    /**
     * 设置区间管理器
     *
     * @param seqRangeMgr 区间管理器
     */
    void setSeqRangeMgr(SeqRangeMgr seqRangeMgr);

    /**
     * 设置获取序列号名称
     *
     * @param name 名称
     */
    void setName(String name);
}
